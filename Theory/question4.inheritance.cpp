#include <iostream>
#include <string>
using std::cin;
using std::cout;
using std::endl;
using std::string;

class Vehicle
{
 public:
     int capacity;
     string size;
     int num_of_pas;

     float isFilled()
     {
         return (float) num_of_pas/capacity*100;
     }
};

class Car: public Vehicle
{
  public:
    string Own_name;
    int cost;
    string Model_name;
    
};

int main()
{
   Car my_car;
   my_car.Own_name="Basu Hela";
   my_car.cost=50000;
   my_car.Model_name="Tesla A1";
   my_car.capacity=4;
   my_car.size="Medium";
   my_car.num_of_pas=1;
   
   cout<<"Owner Name: "<<my_car.Own_name<<endl
       <<"Model Name: "<<my_car.Model_name<<endl
       <<"Car is currently "<< my_car.isFilled()<< "% filled"<<endl;
   return 0;
}




     
