#include <iostream>
using std::cout;
using std::endl;

void swap(int &x,int &y)
{
    x=x+y;
    y=x-y;
    x=x-y;
}
void swap(float &x,float &y)
{
    x=x+y;
    y=x-y;
    x=x-y;
}
void swap(double &x,double &y)
{
    x=x+y;
    y=x-y;
    x=x-y;
}

int main()
{
    int x=5,y=7;
    float a=3.2, b=4.3;
    double d=9.3, e=2.1;
    swap(x,y);
    swap(a,b);
    swap(d,e);
    cout<<"Swapped x="<<x<<"   y="<<y<<endl
        <<"Swapped a="<<a<<"   b="<<b<<endl
        <<"Swapped d="<<d<<"   e="<<e<<endl;
    return 0;
}
    
