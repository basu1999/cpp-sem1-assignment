/* Question: Write the definition for a class called Distance that has data member feet integer and inches as float. The class has the following member function:
  void set(int,float) to give a value to object
  void disp() to display distance in feet and inches
  Distance add(Distance) to sum two distances & return distance

  (a) Write the definitions for each of the above member functions.
  (b) Write main function to create three Distance objects. Set the value in two objects and call add() to calculate sum and assign it in third object. Display all distances.
 */
#include <iostream>
#include <cmath>
using std::cout;
using std::cin;
using std::endl;
using std::fmod;

class Distance 
{
private:
    int feet;
    float inches;
public:
    Distance()
    {
        feet=0;
        inches=0;
    }// Default constructor;

    Distance(int f, float i)
    {
        feet=f;
        inches=i;
    }

    void operator =(const Distance &dist)
    {
        feet=dist.feet;
        inches=dist.inches;
    }

    void set (int f, float i)
    {
        feet=f;
        inches=i;
    }

    void disp()
    {
        cout<<"feet: "<<feet
            <<" Inches: "<<inches<<endl<<endl;
    }

    Distance add(Distance obj)
    {
        Distance dist;
        int temp;
        dist.inches=inches+obj.inches;
        temp=dist.inches/12;
        dist.inches= fmod(dist.inches , 12.0);
        dist.feet=feet+obj.feet+temp;
        return dist;
    }
};

int main()
{
    Distance obj1(5,11.7), obj2, obj3;
    obj2.set(5,11.7);
    obj3=obj1.add(obj2);

    cout<<"Distance 1: "<<endl;
    obj1.disp();
    cout<<"Distance 2: "<<endl;
    obj2.disp();
    cout<<"Distance 3: "<<endl;
    obj3.disp();


    return 0;
}
    
    




        



