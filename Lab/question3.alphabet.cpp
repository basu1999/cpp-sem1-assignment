/* Write a user defined function in C++ to read the content from a text file OUT.TXT, count and display the number of alphabets present in it.
 */
#include <iostream>
#include <fstream>
#include <cctype>

using std::ifstream;
using std::cout;
using std::endl;
using std::isalpha;

int count()
{
    ifstream obj;
    obj.open("OUT.TXT");
    char c;
    int count=0;

    while (!obj.eof())
    {
        obj.get(c);
        if (isalpha(c))
            count++;
    }
    obj.close();
    return count;
}

int main()
{
    int cnt=count();
    cout<<"Total number of alphabets: "<<cnt<<endl;
    return 0;
}


